all: reader writer

reader: src/reader.c
	gcc -Wall -o reader src/reader.c

writer: src/writer.c
	gcc -Wall -o writer src/writer.c
