/****************************************************************************
 * @file writer.c
 * @author David Broin <davidmbroin@gmail.com>
 * @date 2020/08/03
 * @brief Named FIFO writer and signal listener
 * @copyright All rights reserved.
 * @copyright @ref [MIT LICENSE](LICENSE.txt)
 ****************************************************************************/

/****************************************************************************
*	Inclusions of public function dependencies                              *
*****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdint.h>
#include <stdbool.h>
#include <signal.h>

/****************************************************************************
*	Definition macros of private constants                                  *
*****************************************************************************/

#define BUFFER_SIZE 50
#define NAMED_PIPE "FIFO"
#define SIGUSER_1 "SIGN:1"
#define SIGUSER_2 "SIGN:2"
#define FIFO_ALREADY_EXIST -1

/****************************************************************************
*	Definitions of private global variables                                  *
*****************************************************************************/

static int32_t fifo;
static bool isRunning;

/****************************************************************************
*	Prototypes (declarations) of private functions                          *
*****************************************************************************/

static void SigUser1Listener();
static void SigUser2Listener();
static void SigIntListener();

/****************************************************************************
*	Main function, program entry point                                      *
*****************************************************************************/

int main(void)
{   
    char output[BUFFER_SIZE + sizeof("DATA:")];
    char input[BUFFER_SIZE];
    int32_t result;

    struct sigaction SigUser1Action;
    struct sigaction SigUser2Action;
    struct sigaction SigIntAction;

    SigUser1Action.sa_handler = &SigUser1Listener;
    SigUser1Action.sa_flags = SA_RESTART;
    sigemptyset(&SigUser1Action.sa_mask);

    SigUser2Action.sa_handler = &SigUser2Listener;
    SigUser2Action.sa_flags = SA_RESTART;
    sigemptyset(&SigUser2Action.sa_mask);

    SigIntAction.sa_handler = &SigIntListener;
    SigIntAction.sa_flags = 0;
    sigemptyset(&SigIntAction.sa_mask);

    sigaction(SIGUSR1, &SigUser1Action, NULL);
    sigaction(SIGUSR2, &SigUser2Action, NULL);
    sigaction(SIGINT, &SigIntAction, NULL);
    
    result = mknod(NAMED_PIPE, S_IFIFO | 0666, 0);
    if (result < FIFO_ALREADY_EXIST)
    {
        printf("Error creating fifo: %d\n", result);
        exit(1);
    }

    fifo = open(NAMED_PIPE, O_WRONLY);
    if (fifo < 0)
    {
        printf("Error opening fifo file: %d\n", fifo);
        exit(1);
    }

    printf("FIFO file succesfully opened\n\n");

    //Loop until SIGINT
    isRunning = true;
    while (isRunning)
    {
        if (NULL != fgets(input, sizeof(input), stdin))
        {
            snprintf(output, sizeof(output), "DATA:%s", input);
            /* Write buffer to named fifo. Strlen - 1 to avoid sending \n char */
            if (write(fifo, output, strlen(output) - 1) == -1)
            {
                perror("write");
            }
        }
    }

    printf("Bye Bye :)\n\n");
    close(fifo);
    
    return 0;
}

static void SigUser1Listener()
{    
    write(fifo, SIGUSER_1, sizeof(SIGUSER_1));
}

static void SigUser2Listener()
{
    write(fifo, SIGUSER_2, sizeof(SIGUSER_2));
}

static void SigIntListener(){
    isRunning = false;
}
