/****************************************************************************
 * @file reader.c
 * @author David Broin <davidmbroin@gmail.com>
 * @date 2020/08/03
 * @brief Named FIFO reader
 * @copyright All rights reserved.
 * @copyright @ref [MIT LICENSE](LICENSE.txt)
 ****************************************************************************/

/****************************************************************************
*	Inclusions of public function dependencies                              *
*****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdint.h>
#include <signal.h>

/****************************************************************************
*	Definition macros of private constants                                  *
*****************************************************************************/

#define LOG_DATA "Log.txt"
#define LOG_SIGNAL "Sign.txt"
#define STR_TERMINATOR '\0'
#define NAMED_PIPE "FIFO"
#define FIFO_ALREADY_EXIST -1

/****************************************************************************
*	Definitions of private global variables                                  *
*****************************************************************************/

/****************************************************************************
*	Prototypes (declarations) of private functions                          *
*****************************************************************************/

/****************************************************************************
*	Main function, program entry point                                      *
*****************************************************************************/

int main(void)
{
    char inputBuffer[50];
    char inputType[4];
    int32_t bytesRead;
    int32_t result;
    int32_t fifo;
    FILE *logData; 
    FILE *logSignal;

    result = mknod(NAMED_PIPE, S_IFIFO | 0666, 0);
    if (result < FIFO_ALREADY_EXIST)
    {
        printf("Error creating fifo: %d\n", result);
        exit(1);
    }

    logData = fopen(LOG_DATA, "a+t");
    logSignal = fopen(LOG_SIGNAL, "a+t");

    if (logData == NULL || logSignal == NULL)
    {
        printf("Error creating file \n");
        //Try to close both, one could be opened
        fclose(logData);
        fclose(logSignal);
        exit(1);
    }

    fifo = open(NAMED_PIPE, O_RDONLY);
    if (fifo < 0)
    {
        printf("Error opening fifo file: %d\n", fifo);
        /*Close openned files*/
        fclose(logData);
        fclose(logSignal);
        exit(1);
    }

    printf("FIFO file succesfully opened\n\n");

    do
    {
        bytesRead = (int32_t) read(fifo, inputBuffer, sizeof(inputBuffer));
        if (bytesRead > 0)
        {
            inputBuffer[bytesRead] = STR_TERMINATOR;
            printf("%s\n", inputBuffer);
            strncpy(inputType, inputBuffer, sizeof(inputType));
            if (strncmp("DATA", inputType, sizeof(inputType)) == 0)
            {
                fprintf(logData, "%s\n", inputBuffer);
            }
            if (strncmp("SIGN", inputType, sizeof(inputType)) == 0)
            {
                fprintf(logSignal, "%s\n", inputBuffer);
            }
        }
    } while (bytesRead > 0);

    fclose(logData);
    fclose(logSignal);
    close(fifo);

    return 0;
}
